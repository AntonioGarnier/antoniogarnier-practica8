#encoding: UTF-8
require 'spec_helper'
require './lib/practica8/list'
require './lib/practica8/DSLdieta'


describe Dieta do 

    context "#Dieta" do
        menu1 = Dieta::Dieta.new("Etiqueta") do
            titulo "Almuerzo"
            ingesta :min => 30, :max => 35
            plato   :descripcion => "Macarrones con salsa de tomate y queso parmesano",
                    :porcion => "1 1/2 cucharon",
                    :gramos => 200
            plato   :descripcion => "Escalope de ternera",
                    :porcion => "1 bistec mediano",
                    :gramos => 100
            plato   :descripcion => "Ensalada basica con zanahoria rallada",
                    :porcion => "guarnicion",
                    :gramos => 120
            plato   :descripcion => "Mandarina", :porcion => "1 grande", :gramos => 180
            plato   :descripcion => "Pan de trigo integral", :porcion => "1 rodaja", :gramos => 20
            porcentajes :vct => 785.9, :proteinas => 19, :grasas => 34, :hidratos => 47
        end
        
        it "El objeto de la clase Dieta pertenece a la jerarquia Dieta" do
            expect(menu1.kind_of?Dieta::Dieta).to eq(true)
        end
        
        it "Se crea el objeto Dieta" do
            expect(menu1.instance_of?Dieta::Dieta).to eq(true)
        end
        
        puts menu1
    end
    
    context "#Alimento" do
        menu2 = Dieta::Alimento.new("Etiqueta") do
            titulo "Cena"
            ingesta :min => 30, :max => 35
            plato   :descripcion => "Macarrones con salsa de tomate y queso parmesano",
                    :porcion => "1 1/2 cucharon",
                    :gramos => 200
            plato   :descripcion => "Escalope de ternera",
                    :porcion => "1 bistec mediano",
                    :gramos => 100
            plato   :descripcion => "Ensalada basica con zanahoria rallada",
                    :porcion => "guarnicion",
                    :gramos => 220
            plato   :descripcion => "Mandarina", :porcion => "1 pequeno", :gramos => 180
            plato   :descripcion => "Pan de trigo integral", :porcion => "1 rodaja", :gramos => 20
            porcentajes :vct => 500.9, :proteinas => 19, :grasas => 44, :hidratos => 47
            sano "Verduritas sanas"
        end
        
        it "El objeto de la clase Alimento pertenece a la jerarquia Dieta" do
            expect(menu2.kind_of?Dieta::Dieta).to eq(true)
        end
        
        it "Se crea el objeto Alimento" do
            expect(menu2.instance_of?Dieta::Alimento).to eq(true)
        end
        
        puts menu2
    end
        
    context "#Edad" do
        menu3 = Dieta::Edad.new("Etiqueta") do
            titulo "Merienda"
            ingesta :min => 35, :max => 50
            plato   :descripcion => "Macarrones con salsa de tomate y queso parmesano",
                    :porcion => "1 1/2 cucharon",
                    :gramos => 100
            plato   :descripcion => "Escalope de ternera",
                    :porcion => "1 bistec mediano",
                    :gramos => 100
            plato   :descripcion => "Ensalada basica con zanahoria rallada",
                    :porcion => "guarnicion",
                    :gramos => 220
            plato   :descripcion => "Mandarina", :porcion => "1 pequeno", :gramos => 150
            plato   :descripcion => "Pan de trigo integral", :porcion => "1 rodaja", :gramos => 20
            porcentajes :vct => 500.9, :proteinas => 12, :grasas => 24, :hidratos => 57
            range "De 9 a 13 años"
        end
        
        it "El objeto de la clase Edad pertenece a la jerarquia Dieta" do
            expect(menu3.kind_of?Dieta::Dieta).to eq(true)
        end
        
        it "Se crea el objeto Edad" do
            expect(menu3.instance_of?Dieta::Edad).to eq(true)
        end
        puts menu3
    end

end
