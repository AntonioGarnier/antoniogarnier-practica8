module Dieta
    class Dieta
            
        attr_accessor :cabecera, :descrip, :vct
        
        include Comparable
    
        def initialize(etiqueta, &block)
            self.cabecera = []
            self.descrip = []
            self.vct = []
        
            if block_given?  
                if block.arity == 1
                    yield self
                else
                    instance_eval &block 
                end
            end
        end
      
        def <=> (otro)
            @vct[0] <=> otro.vct[0]
        end
        
        def to_s
    
            out = get_titulo
            out << get_platos
            out << get_footer
            out
            
        end
        
        def get_titulo
    
            out ="#{@cabecera[0]}"
            out << get_porc_inges_diaria
            out        
        end
    
        def get_porc_inges_diaria
    
            out = " ("
            out << "#{@cabecera[1]}"
            out << "% - "
            out << "#{@cabecera[2]}"
            out << "%)\n"
            out
        end
        
        def get_plato(i)
    
            out = "#{@descrip[i]}"
            out
        end
    
        def get_platos
    
            out=""
            i=0
            @descrip.each do
                out << "- "
                out << get_plato(i)
                out << "\n"
                i+=1
            end
            out    
        end
        
        def get_vct
    
            out = "V.C.T. | %    "
            out << "#{@vct[0]}" 
            out << " kcal | "
            out       
        end
    
        def get_porc_prot
    
            out = "#{@vct[1]}" 
            out << "% - "
            out       
        end
    
        def get_porc_gras
    
            out = "#{@vct[2]}" 
            out << "% - "
            out       
        end
    
        def get_porc_hdc
    
            out = "#{@vct[3]}" 
            out << "%"
            out       
        end
        
        def get_footer
    
            out = get_vct
            out << get_porc_prot
            out << get_porc_gras
            out << get_porc_hdc
            out 
        end
      
      def titulo (title)
          @cabecera[0] = title
      end
      
      def ingesta (options = {})
        @cabecera[1] = " (#{options[:min]})" if options[:min]
        @cabecera[2] = " (#{options[:max]})" if options[:max]
      end
      
      def plato (options = {})
        describe = " (#{options[:descripcion]})" if options[:descripcion]
        describe <<  " (#{options[:porcion]})" if options[:porcion]
        describe <<  " (#{options[:gramos]})" if options[:gramos]
        @descrip << describe
      end
      
      def porcentajes (options = {})
        @vct[0] = " (#{options[:vct]})" if options[:vct]
        @vct[1] =  " (#{options[:proteinas]})" if options[:proteinas]
        @vct[2] =  " (#{options[:grasas]})" if options[:grasas]
        @vct[3] =  " (#{options[:hidratos]})" if options[:hidratos]
      end
      
    end
    
    class Alimento < Dieta
        attr_accessor :saludable
        
        def initialize(etiqueta, &block)
            self.cabecera = []
            self.descrip = []
            self.vct = []
            self.saludable = []
        
            if block_given?  
                if block.arity == 1
                    yield self
                else
                    instance_eval &block 
                end
            end
        end
        
        def salud()
            out = "\nGrupo de alimentos: #{@saludable}"
            out
        end
        
        def to_s
            out = super.to_s
            out << salud()
            out
        end
        
        
        def sano (text)
           @saludable = text 
        end
        
    end
    
    class Edad < Dieta
        attr_accessor :rango_edad
        
        def initialize(etiqueta, &block)
            self.cabecera = []
            self.descrip = []
            self.vct = []
            self.rango_edad = []
        
            if block_given?  
                if block.arity == 1
                    yield self
                else
                    instance_eval &block 
                end
            end
        end
        
        def rango()
            out = "\nIntervalos de edad: #{@rango_edad}"
            out
        end
        
        def to_s
            out = super.to_s
            out << rango()
            out
        end   
        
        def range (text)
           @rango_edad = text 
        end
        
    end
end 
